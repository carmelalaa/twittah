class UsersController < ApplicationController
  def users
    @user = User.all
  end

  def show
    @user = User.find(params[:id])
    @user_tweets = @user.tweets.order(created_at: :desc)
  end

  def destroy
   @user = User.find(params[:id])

   if @user.destroy
       redirect_to root_url, notice: "User deleted."
   end
 end

end
