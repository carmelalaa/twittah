class TweetsController < ApplicationController

  before_action :find_tweet, only: [:destroy, :show]

  def create
    @tweet = Tweet.new(tweet_params.merge(user_id: current_user.id))
    @tweet.save

    redirect_to new_tweet_path
  end

  def new
    redirect_to tweets_path
  end

  def destroy
    @tweet.destroy

    redirect_to tweets_path
  end

  def index
    @tweet = Tweet.new
    @tweets = Tweet.order(created_at: :desc)
  end

  def show
  end

  private

  def tweet_params
    params.require(:tweet).permit(:content)
  end

  def find_tweet
    @tweet = Tweet.find(params[:id])
  end
end
