module UsersHelper
  def display_name(user)
    user == current_user ? 'Me' : user.email
  end
end
