Rails.application.routes.draw do
  devise_for :users
  root to: "home#index"
  get '/users', to: 'users#users', as: 'users'
  resources :tweets
  resources :relationships
  resources :users do
    member do
      get :following, :followers
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

# get '/url', to: 'controller#method' as: 'some term'
